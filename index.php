<?php
    require_once('controllers/materia.controller.php');

    $materiasController = new MateriaController();
    if(isset($_GET['action'])){
      switch ($_GET['action']) {
        case 'insert':
          $materiasController->insertMateria();
          break;
        default:
          $materiasController->showMateria();
          break;
      }
    }else
      $materiasController->showMateria();
