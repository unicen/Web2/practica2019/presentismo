<?php

class MateriaModel{
    private $db;

    public function __construct(){
        $this->db = new PDO('mysql:host=localhost;dbname=db_presentismo;charset=utf8', 'root', '');
    }

    function get($id){
    $query = $this->db->prepare('SELECT * FROM materia WHERE id = :id');

        // ejecuto la consulta
        $ok = $query->execute(['id'=>$id]);
        if(!$ok){
             var_dump($query->errorInfo());
            die();
        }
        // obtengo la respuesta
        $materia = $query->fetch(PDO::FETCH_OBJ);

        return $materia;
    }

    function getAll() {
        // preparo la consulta
        $query = $this->db->prepare('SELECT * FROM materia');

        // ejecuto la consulta
        $ok = $query->execute();
        if(!$ok){
             var_dump($query->errorInfo());
            die();
        }
        // obtengo la respuesta
        $materias = $query->fetchAll(PDO::FETCH_OBJ);

        return $materias;
    }

    function insert($nombre, $docente, $img){
      $query = $this->db->prepare('INSERT INTO materia(nombre, docente, img) VALUES (:nombre, :docente, :img)');

      // ejecuto la consulta
      $ok = $query->execute(['nombre'=>$nombre, 'img'=>$img, 'docente'=>$docente]);
      if(!$ok){
           var_dump($query->errorInfo());
          die();
      }
    }

}
